#!/bin/sh
echo "Welcome to the ewfm automagic"
echo "website setter-upper wizard!!"

echo "First, let's start by creating input/ and the default index.md file."
echo .
mkdir input
touch input/index.md
echo "Your website!" | tee -a input/index.md
echo "## Hello, world!" | tee -a input/index.md
echo "Welcome to ewfm!" | tee -a input/index.md
echo .
echo "Done! Now let's configure your site.conf file!"

touch input/site.conf

echo "What is your website's domain?"
read domain
echo $domain | tee -a input/site.conf

echo "What is your website's name?"
echo .
read name
echo $name | tee -a input/site.conf

echo "What theme would you like to use? (If in doubt, type linear-base or 3box-base)"
echo .
read theme
echo $theme | tee -a input/site.conf

echo "All done!"
echo "If you haven't already read the docs at ewfm.colean.cc/docs"
echo "and run populate.sh to download all the ewfm themes!"

echo ".jpeg" | tee -a input/inclusions.conf
echo ".jpg" | tee -a input/inclusions.conf
echo ".gif" | tee -a input/inclusions.conf
echo ".png" | tee -a input/inclusions.conf
echo ".bmp" | tee -a input/inclusions.conf
echo ".webp" | tee -a input/inclusions.conf
echo ".webm" | tee -a input/inclusions.conf
echo ".txt" | tee -a input/inclusions.conf
echo ".rtf" | tee -a input/inclusions.conf
echo ".doc" | tee -a input/inclusions.conf
echo ".xls" | tee -a input/inclusions.conf
echo ".docx" | tee -a input/inclusions.conf
echo ".xlsx" | tee -a input/inclusions.conf
echo ".zip" | tee -a input/inclusions.conf
echo ".tar" | tee -a input/inclusions.conf
echo ".gz" | tee -a input/inclusions.conf
echo ".tgz" | tee -a input/inclusions.conf
echo ".txz" | tee -a input/inclusions.conf
echo ".lzma" | tee -a input/inclusions.conf
echo ".xz" | tee -a input/inclusions.conf
echo ".bz2" | tee -a input/inclusions.conf
echo ".bzip2" | tee -a input/inclusions.conf
echo ".tbz" | tee -a input/inclusions.conf
echo ".tbz2" | tee -a input/inclusions.conf

mkdir output
