require 'redcarpet'
require 'date'

site = IO.readlines("input/site.conf", chomp: true)
site_domain = site[0]
site_name = site[1]
theme = site[2]

directory = Dir["input/*/"]
everything = Dir["input/**/*.md"]
workspace = Dir["input/*.md"]
prelim = Dir["input/**/*.*"]
base = Dir["input/**/"]
files = []
structure = []

workspace.push(*everything)

themes_fold = "themes/"
theme_dir = themes_fold + theme + "/"

protocol = "https://"

da = ["<a href=\"" + protocol + site_domain + "\">home</a> "]
dp = ["<p><a href=\"" + protocol + site_domain + "\">home</a></p>"]
dsa = ["<a href=\"" + protocol + site_domain + "\">/</a> "]
dsp = ["<p><a href=\"" + protocol + site_domain + "\">/</a></p>"]

Dir.mkdir "output/ewfm_theme"

dist_files = Dir[theme_dir + "dist/*"]
puts dist_files
for file in dist_files
	string = File.read(file)
	
	handle = file
	
	prefix = theme_dir + "dist/"
	handle[prefix]= "output/ewfm_theme/"

	IO.write(handle, string)
end

for string in base
	if string != "input/"
		structure.append(string)
	end
end

for string in prelim
	if not string.include? ".md"
        	files.append(string)
	end
end

procfiles = Marshal.load(Marshal.dump(files))

for file in directory
	file1 = "/" + file
	file1["input/"]= ""
	file2 = file1[1..-1]
	file2["/"]= ""
	da.append("<a href=\"" + protocol + site_domain + file1 + "\">" + file2 + "</a> ")
	dp.append("<p><a href=\"" + protocol + site_domain + file1 + "\">" + file2 + "</a></p>")
	dsa.append("<a href=\"" + protocol + site_domain + file1 + "\">" + file2 + "/</a> ")
	dsp.append("<p><a href=\"" + protocol + site_domain + file1 + "\">" + file2 + "/</a></p>")
end 

for file in structure
	handle = file
	handle["input/"]= ""

	comb = "output/" + handle
	Dir.mkdir comb unless Dir.exists? comb
end

for file in files
	if(File.exist?('input/inclusions.conf'))
		list = IO.readlines("input/inclusions.conf", chomp: true)
		for ext in list
			if file.include? ext
				handle = File.read(file)

				output = file
				output["input"]= "output"
				IO.write(output, handle)
			end
		end
	else
		handle = File.read(file)

		output = file
		output["input"]= "output"
		IO.write(output, handle)
	end
end

def scope
	yield
end

if(File.exist?('input/preproc.conf'))
	list = IO.readlines("input/preproc.conf", chomp: true)
	for proc in list
		if(File.exist?('proc/' + proc +'/pre.sh'))
			for file in procfiles
				runner = `proc/#{proc}/pre.sh #{file}`
				puts runner
			end
		else
			puts proc +" does not support being used as a pre-processor."
		end
	end
end

for file in workspace
	text = File.read(file)
	template = File.read(theme_dir + "template.html")

        product = template

	puts file

	renderer = Redcarpet::Markdown.new(Redcarpet::Render::HTML, autolink: true, fenced_code_blocks: true, strikethrough: true, underline: true, space_after_headers: true)
	lines = text.lines
	rest_of_content = lines[1..-1]
	raw_content = rest_of_content.join()
	title = lines[0]

	content = renderer.render(raw_content)

	if product.include? "{SITE_NAME}"
	while product.include? "{SITE_NAME}"
		product["{SITE_NAME}"]= site_name
	end
	end 

	if product.include? "{SITE_DOMAIN}"
	while product.include? "{SITE_DOMAIN}"
		product["{SITE_DOMAIN}"]= site_domain
	end
	end

	if product.include? "{PAGE_TITLE}"
	while product.include? "{PAGE_TITLE}"
		product["{PAGE_TITLE}"]= title
	end
	end 

	if product.include? "{DIRECTORY_A}"
	while product.include? "{DIRECTORY_A}"
		product["{DIRECTORY_A}"]= da.join()
	end
	end 

	if product.include? "{DIRECTORY_P}"
	while product.include? "{DIRECTORY_P}"
		product["{DIRECTORY_P}"]= dp.join()
	end
	end 

	if product.include? "{DIRECTORY_SLASH_A}"
	while product.include? "{DIRECTORY_SLASH_A}"
		product["{DIRECTORY_SLASH_A}"]= dsa.join()
	end
	end 

	if product.include? "{DIRECTORY_SLASH_P}"
	while product.include? "{DIRECTORY_SLASH_P}"
		product["{DIRECTORY_SLASH_P}"]= dsp.join()
	end
	end 

	if product.include? "{PAGE_CONTENT}"
	while product.include? "{PAGE_CONTENT}"
		product["{PAGE_CONTENT}"]= content
	end
	end 

	if product.include? "$EWFM$"
	while product.include? "$EWFM$"
		product["$EWFM$"]= "<p>This page has been generated by ewfm using the " + theme + " theme!</p>"
	end
	end 

	if product.include? "$THEME$"
	while product.include? "$THEME$"
		product["$THEME$"]= theme
	end
	end

	if product.include? "$TIME$"
	while product.include? "$TIME$"
		time = DateTime.now
		cdt = time.strftime "%d/%m/%Y %H:%M:%S"
		product["$TIME$"]= cdt
	end
	end

	if product.include? "$RUBY$"
	while product.include? "$RUBY$"
		product["$RUBY$"]= RUBY_VERSION 
	end
	end

	handle = file
	handle["input"]= "output"
	handle[".md"]= ".html"
	IO.write(handle, product)
end

if(File.exist?('input/postproc.conf'))
	list = IO.readlines("input/postproc.conf", chomp: true)
	for proc in list
		if(File.exist?('proc/' + proc +'/post.sh'))
			runner = `proc/#{proc}/post.sh`
			puts runner
		else
			puts proc +" does not support being used as a post-processor."
		end
	end
end
