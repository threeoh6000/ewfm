#!/bin/sh
git clone https://gitlab.com/threeoh6000/ewfm-themes.git

mkdir themes

mv ewfm-themes/linear/* themes/
mv ewfm-themes/3box/* themes/
mv ewfm-themes/roundbox/* themes/
mv ewfm-themes/other/* themes/

mv ewfm-themes/LICENSE themes/STANDARD-LICENSE
mv ewfm-themes/README themes/STANDARD-README

rm -rf ewfm-themes/
